package io.niceday.geniesampleapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenieSampleAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenieSampleAppApplication.class, args);
    }

}
