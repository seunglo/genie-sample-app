package io.niceday.geniesampleapp.album.controller;

import io.niceday.geniesampleapp.album.dto.AlbumDetail;
import io.niceday.geniesampleapp.album.dto.AlbumForm;
import io.niceday.geniesampleapp.album.dto.AlbumSearchCondition;
import io.niceday.geniesampleapp.album.dto.AlbumSummary;
import io.niceday.geniesampleapp.album.service.AlbumService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RequestMapping("/albums")
@RestController
public class AlbumController {

    private final AlbumService albumService;

    @GetMapping("/{id}")
    public AlbumDetail get(@PathVariable Long id) {
        return albumService.getAlbumDetail(id);
    }

    @GetMapping
    public Page<AlbumSummary> getPage(AlbumSearchCondition condition, Pageable pageable) {
        return albumService.searchAlbums(condition, pageable);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Long create(@RequestBody @Validated AlbumForm form) {
        return albumService.createAlbum(form);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody @Validated AlbumForm form) {
        albumService.updateAlbum(id, form);
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable Long id) {
        albumService.removeAlbum (id);
    }
}
