package io.niceday.geniesampleapp.album.mapper;

import io.niceday.geniesampleapp.album.dto.AlbumForm;
import io.niceday.geniesampleapp.album.model.Album;
import io.niceday.geniesampleapp.album.model.Soundtrack;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AlbumMapper {
    public Album toAlbum(AlbumForm albumForm, Long id) {
        return Album.builder()
                .id(id)
                .name(albumForm.getName())
                .genre(albumForm.getGenre())
                .releaseDate(albumForm.getReleaseDate())
                .artistId(albumForm.getArtistId())
                .description(albumForm.getDescription())
                .build();
    }

    public Album toAlbum(AlbumForm albumForm) {
        return Album.builder()
                .name(albumForm.getName())
                .genre(albumForm.getGenre())
                .releaseDate(albumForm.getReleaseDate())
                .artistId(albumForm.getArtistId())
                .description(albumForm.getDescription())
                .build();
    }

    public List<Soundtrack> toSoundtracks(AlbumForm albumForm, Long albumId) {
        return albumForm
                .getSoundtracks()
                .stream()
                .map(soundtrackForm ->
                        Soundtrack.builder()
                                .playTime(soundtrackForm.getPlayTime())
                                .order(soundtrackForm.getOrder())
                                .name(soundtrackForm.getName())
                                .isDisplay(soundtrackForm.getIsDisplay())
                                .albumId(albumId)
                                .build()
                )
                .collect(Collectors.toList());
    }
}
