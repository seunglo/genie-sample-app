package io.niceday.geniesampleapp.album.service;

import io.niceday.geniesampleapp.album.dto.AlbumDetail;
import io.niceday.geniesampleapp.album.dto.AlbumForm;
import io.niceday.geniesampleapp.album.dto.AlbumSearchCondition;
import io.niceday.geniesampleapp.album.dto.AlbumSummary;
import io.niceday.geniesampleapp.album.mapper.AlbumMapper;
import io.niceday.geniesampleapp.album.model.Album;
import io.niceday.geniesampleapp.album.model.Soundtrack;
import io.niceday.geniesampleapp.album.repository.AlbumRepository;
import io.niceday.geniesampleapp.album.repository.SoundtrackRepository;
import io.niceday.geniesampleapp.common.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Transactional
@Service
public class AlbumService {

    private final AlbumRepository albumRepository;

    private final SoundtrackRepository soundtrackRepository;

    private final AlbumMapper albumMapper;

    public AlbumDetail getAlbumDetail(Long id) {
        return albumRepository.findAlbumDetail(id)
                .orElseThrow(() -> new ResourceNotFoundException("album", "id", id));
    }

    public Page<AlbumSummary> searchAlbums(AlbumSearchCondition condition, Pageable pageable) {
        List<AlbumSummary> albums = albumRepository.findAll(condition, pageable);
        return new PageImpl<>(albums, pageable, albumRepository.count(condition));
    }

    public Long createAlbum(AlbumForm form) {
        Album source = albumMapper.toAlbum(form);
        albumRepository.save(source);

        List<Soundtrack> soundtracks = albumMapper.toSoundtracks(form, source.getId());
        if (!soundtracks.isEmpty()) {
            soundtrackRepository.saveAll(soundtracks);
        }

        return source.getId();
    }

    public void updateAlbum(Long id, AlbumForm form) {
        findAlbumOrElseThrow(id);

        albumRepository.update(albumMapper.toAlbum(form, id));

        replaceSoundtracks(id, form);
    }

    public void removeAlbum(Long id) {
        findAlbumOrElseThrow(id);

        albumRepository.remove(id);
    }

    private Album findAlbumOrElseThrow(Long id) {
        return albumRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("album", "id", id));
    }

    private void replaceSoundtracks(Long albumId, AlbumForm form) {
        soundtrackRepository.removeByAlbumId(albumId);

        List<Soundtrack> soundtracks = albumMapper.toSoundtracks(form, albumId);
        if (!soundtracks.isEmpty()) {
            soundtrackRepository.saveAll(soundtracks);
        }
    }
}
