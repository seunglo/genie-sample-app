package io.niceday.geniesampleapp.album.dto;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class AlbumDetail {

    private Long id;

    private String name;

    private String genre;

    private LocalDate releaseDate;

    private String description;

    private List<Soundtrack> soundtracks;

    private Artist artist;

    private String createdBy;

    private LocalDateTime createdDateTime;

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Getter
    @Setter
    public static class Soundtrack {

        private Long id;

        private String name;

        private Integer order;

        private Boolean isDisplay;

        private LocalTime playTime;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Getter
    @Setter
    public static class Artist {

        private Long id;

        private String name;
    }
}
