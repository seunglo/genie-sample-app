package io.niceday.geniesampleapp.album.dto;

import io.niceday.geniesampleapp.album.model.Album;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter @Setter
public class AlbumForm {

    @NotBlank
    private String name;

    private String genre;

    @NotNull
    private LocalDate releaseDate;

    private String description;

    @NotNull
    private Long artistId;

    @Valid
    @Builder.Default
    private List<Soundtrack> soundtracks = new ArrayList<>();

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Getter
    public static class Soundtrack {

        @NotBlank
        private String name;

        @NotNull
        private Integer order;

        @Builder.Default
        private Boolean isDisplay = true;

        @NotNull
        private LocalTime playTime;

    }
}
