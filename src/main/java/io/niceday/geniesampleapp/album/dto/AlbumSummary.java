package io.niceday.geniesampleapp.album.dto;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter @Setter
public class AlbumSummary {

    private Long id;

    private String name;

    private String genre;

    private LocalDate releaseDate;

    private String description;

    private Artist artist;

    private String createdBy;

    private LocalDateTime createdDateTime;

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Getter @Setter
    public static class Artist {

        private Long id;

        private String name;
    }
}
