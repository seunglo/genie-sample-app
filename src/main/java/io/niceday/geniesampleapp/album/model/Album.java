package io.niceday.geniesampleapp.album.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Album {

    private Long id;

    private String name;

    private String genre;

    private LocalDate releaseDate;

    private String description;

    private Long artistId;

    private String createdBy;

    private LocalDateTime createdDateTime;
}
