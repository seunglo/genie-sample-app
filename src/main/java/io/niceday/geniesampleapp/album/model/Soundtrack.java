package io.niceday.geniesampleapp.album.model;

import lombok.*;

import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class Soundtrack {

    private Long id;

    private String name;

    private Integer order;

    private Boolean isDisplay;

    private LocalTime playTime;

    private Long albumId;
}
