package io.niceday.geniesampleapp.album.repository;

import io.niceday.geniesampleapp.album.dto.AlbumDetail;
import io.niceday.geniesampleapp.album.dto.AlbumSearchCondition;
import io.niceday.geniesampleapp.album.dto.AlbumSummary;
import io.niceday.geniesampleapp.album.model.Album;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@Mapper
public interface AlbumRepository {

    Optional<AlbumDetail> findAlbumDetail(Long id);

    Optional<Album> findById(Long id);

    List<AlbumSummary> findAll(@Param("condition") AlbumSearchCondition condition,
                               @Param("page") Pageable pageable);

    Integer count(@Param("condition") AlbumSearchCondition condition);

    void save(Album album);

    void update(Album album);

    void remove(Long id);
}
