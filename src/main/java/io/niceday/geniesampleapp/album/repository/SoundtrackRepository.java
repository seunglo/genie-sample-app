package io.niceday.geniesampleapp.album.repository;

import io.niceday.geniesampleapp.album.model.Soundtrack;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SoundtrackRepository {

    void saveAll(List<Soundtrack> soundtracks);

    void removeByAlbumId(Long albumId);
}
