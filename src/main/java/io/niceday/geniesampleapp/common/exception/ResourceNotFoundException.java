package io.niceday.geniesampleapp.common.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String resourceName, String fieldName, Object fieldValue) {
        super(String.format("%s가 %s인 %s는 존재하지 않습니다", fieldName, fieldValue, resourceName));
    }
}
