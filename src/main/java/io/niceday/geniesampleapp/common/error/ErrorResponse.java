package io.niceday.geniesampleapp.common.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
public class ErrorResponse {

    public String message;

    public List<ErrorDetail> errors;

    public ErrorResponse(String message) {
        this.message = message;
    }

    public ErrorResponse(String message, List<ErrorDetail> errors) {
        this.message = message;
        this.errors = errors;
    }

    @AllArgsConstructor
    @Getter
    public static class ErrorDetail {
        private String field;

        private String description;
    }
}
