package io.niceday.geniesampleapp.artist.repository;

import io.niceday.geniesampleapp.artist.dto.ArtistSearchCondition;
import io.niceday.geniesampleapp.artist.model.Artist;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@Mapper
public interface ArtistRepository {
    Optional<Artist> findById(Long id);

    List<Artist> findAll(@Param("condition") ArtistSearchCondition condition,
                         @Param("page") Pageable pageable);

    Integer count(@Param("condition") ArtistSearchCondition condition);

    void save(Artist artist);

    void remove(Long id);

    void update(Artist artist);
}
