package io.niceday.geniesampleapp.artist.service;

import io.niceday.geniesampleapp.artist.dto.ArtistForm;
import io.niceday.geniesampleapp.artist.dto.ArtistSearchCondition;
import io.niceday.geniesampleapp.artist.mapper.ArtistMapper;
import io.niceday.geniesampleapp.artist.model.Artist;
import io.niceday.geniesampleapp.artist.repository.ArtistRepository;
import io.niceday.geniesampleapp.common.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Transactional
@Service
public class ArtistService {

    private final ArtistRepository artistRepository;

    private final ArtistMapper artistMapper;

    public Artist getArtist(Long id) {
        return findArtist(id);
    }

    public Page<Artist> searchArtists(ArtistSearchCondition condition, Pageable pageable) {
        List<Artist> artists = artistRepository.findAll(condition, pageable);
        return new PageImpl<>(artists, pageable, artistRepository.count(condition));
    }

    public Long createArtist(ArtistForm form) {
        Artist source = artistMapper.toArtist(form);
        artistRepository.save(source);
        return source.getId();
    }

    public void updateArtist(Long id, ArtistForm form) {
        findArtist(id);
        artistRepository.update(artistMapper.toArtist(form, id));
    }

    public void removeArtist(Long id) {
        findArtist(id);
        artistRepository.remove(id);
    }

    private Artist findArtist(Long id) {
        return artistRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("artist", "id", id));
    }
}
