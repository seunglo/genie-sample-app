package io.niceday.geniesampleapp.artist.controller;

import io.niceday.geniesampleapp.artist.dto.ArtistForm;
import io.niceday.geniesampleapp.artist.dto.ArtistSearchCondition;
import io.niceday.geniesampleapp.artist.model.Artist;
import io.niceday.geniesampleapp.artist.service.ArtistService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RequestMapping("/artists")
@RestController
public class ArtistController {

    private final ArtistService artistService;

    @GetMapping("/{id}")
    public Artist get(@PathVariable Long id) {
        return artistService.getArtist(id);
    }

    @GetMapping
    public Page<Artist> getPage(ArtistSearchCondition condition, Pageable pageable) {
        return artistService.searchArtists(condition, pageable);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public void create(@RequestBody @Validated ArtistForm form) {
        artistService.createArtist(form);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody @Validated ArtistForm form) {
        artistService.updateArtist(id, form);
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable Long id) {
        artistService.removeArtist(id);
    }
}
