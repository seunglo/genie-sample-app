package io.niceday.geniesampleapp.artist.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class ArtistForm {

    @NotBlank
    private String name;

    @NotBlank
    private String country;

    private String agency;

    private String description;

    @NotNull
    private LocalDate birthDate;

}
