package io.niceday.geniesampleapp.artist.mapper;

import io.niceday.geniesampleapp.artist.dto.ArtistForm;
import io.niceday.geniesampleapp.artist.model.Artist;
import org.springframework.stereotype.Component;

@Component
public class ArtistMapper {
    public Artist toArtist(ArtistForm artistForm) {
        return Artist.builder()
                .name(artistForm.getName())
                .country(artistForm.getCountry())
                .agency(artistForm.getAgency())
                .description(artistForm.getDescription())
                .birthDate(artistForm.getBirthDate())
                .build();
    }

    public Artist toArtist(ArtistForm artistForm, Long id) {
        return Artist.builder()
                .id(id)
                .name(artistForm.getName())
                .country(artistForm.getCountry())
                .agency(artistForm.getAgency())
                .description(artistForm.getDescription())
                .birthDate(artistForm.getBirthDate())
                .build();
    }
}
