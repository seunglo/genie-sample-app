package io.niceday.geniesampleapp.artist.model;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class Artist {

    private Long id;

    private String name;

    private String country;

    private String agency;

    private String description;

    private LocalDate birthDate;

    private String createdBy;

    private LocalDateTime createdDateTime;
}
