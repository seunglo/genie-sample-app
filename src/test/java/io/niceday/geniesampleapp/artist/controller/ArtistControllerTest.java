package io.niceday.geniesampleapp.artist.controller;

import io.niceday.geniesampleapp.artist.dto.ArtistForm;
import io.niceday.geniesampleapp.artist.service.ArtistService;
import io.niceday.geniesampleapp.common.AbstractBaseControllerTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.time.LocalDate;
import java.util.stream.IntStream;

import static io.niceday.geniesampleapp.artist.fixture.ArtistFixtures.aArtistForm;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ArtistControllerTest extends AbstractBaseControllerTest {

    @Autowired
    private ArtistService artistService;

    @Nested
    @DisplayName("GET /artists/{id} 요청은")
    class Describe_get {

        @Nested
        @DisplayName("정상적인 요청이 오면")
        class Context_when_valid_request {
            Long id;

            @BeforeEach
            void setUp() {
                id = artistService.createArtist(aArtistForm());
            }

            @Test
            @DisplayName("artist를 응답한다.")
            void it_responses_artist() throws Exception {
                mockMvc.perform(get("/artists/{id}", id))
                        .andExpect(status().isOk())
                        .andDo(document("get-artist",
                                responseFields(
                                        fieldWithPath("id").description("아이디"),
                                        fieldWithPath("name").description("이름"),
                                        fieldWithPath("birthDate").description("생일"),
                                        fieldWithPath("country").description("국적"),
                                        fieldWithPath("agency").description("소속사").optional(),
                                        fieldWithPath("description").description("설명").optional(),
                                        fieldWithPath("createdBy").description("등록자"),
                                        fieldWithPath("createdDateTime").description("등록일시")
                                )
                        ));
            }
        }

        @Nested
        @DisplayName("artist가 존재하지 않으면")
        class Context_when_artist_does_not_exist {

            @Test
            @DisplayName("404 status를 응답한다.")
            void it_responses_artist() throws Exception {
                mockMvc.perform(get("/artists/{id}", 3343))
                        .andExpect(status().isNotFound());
            }
        }
    }

    @Nested
    @DisplayName("GET /artists 요청은")
    class Describe_page {

        @Nested
        @DisplayName("30개의 데이터가 주어지고 Page 파라미터의 페이지가 0, 사이즈가 10이면")
        class Context_pageable {

            @BeforeEach
            void setUp() {
                IntStream.range(0, 30)
                        .forEach((i) -> artistService.createArtist(aArtistForm("아이유" + i)));
            }

            @Test
            @DisplayName("10개의 Artist가 있는 page를 응답한다.")
            void it_response_artists_page() throws Exception {
                mockMvc.perform(get("/artists")
                                .queryParam("page", "1")
                                .queryParam("size", "10")
                                .queryParam("name", "아이유")
                        )
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("content.length()").value(10))
                        .andExpect(jsonPath("totalElements").value(30))
                        .andDo(document("page-artist",
                                requestParameters(
                                        parameterWithName("page").description("페이지 번호 (default 0)").optional(),
                                        parameterWithName("size").description("페이지 사이즈 (default 20)").optional(),
                                        parameterWithName("name").description("이름 검색 파라미터").optional()
                                )
                        ));
            }
        }

        @Nested
        @DisplayName("3개의 데이터가 주어지고 name 파라미터가 아이유 이면")
        class Context_condition {

            @BeforeEach
            void setUp() {
                artistService.createArtist(aArtistForm("아이유"));
                artistService.createArtist(aArtistForm("하이유"));
                artistService.createArtist(aArtistForm("무야호"));
            }

            @Test
            @DisplayName("아이유가 포함된 이름을 가진 Artist가 있는 page를 응답한다.")
            void it_response_artists_page() throws Exception {
                mockMvc.perform(get("/artists")
                                .queryParam("name", "아이유")
                        )
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("content.length()").value(1))
                        .andExpect(jsonPath("content[0].name").value("아이유"));
            }
        }
    }

    @Nested
    @DisplayName("POST /artists 요청은")
    class Describe_create {

        @Nested
        @DisplayName("정상적인 요청이 오면")
        class Context_when_valid_request {
            ArtistForm artistForm;

            @BeforeEach
            void setUp() {
                artistForm = ArtistForm.builder()
                        .name("하이유")
                        .agency("JPA")
                        .country("대한민국")
                        .birthDate(LocalDate.of(1995, 03, 29))
                        .description("안녕하세요 하이유 입니다.")
                        .build();
            }

            @Test
            @DisplayName("201 상태코드를 응답한다.")
            void it_responses_201_status() throws Exception {
                mockMvc.perform(post("/artists")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(toJson(artistForm))
                        )
                        .andExpect(status().isCreated())
                        .andDo(document("create-artist",
                                requestFields(
                                        fieldWithPath("name").description("이름"),
                                        fieldWithPath("birthDate").description("생일"),
                                        fieldWithPath("country").description("국적"),
                                        fieldWithPath("agency").description("소속사").optional(),
                                        fieldWithPath("description").description("설명").optional()
                                )
                        ));
            }

        }
    }

    @Nested
    @DisplayName("PUT /artists/{id} 요청은")
    class Describe_update {

        @Nested
        @DisplayName("정상적인 요청이 오면")
        class Context_with_valid_request {
            Long id;

            ArtistForm artistForm;

            @BeforeEach
            void setUp() {
                id = artistService.createArtist(aArtistForm());

                artistForm = ArtistForm.builder()
                        .name("아이유")
                        .agency("EDAM 엔터테인먼트")
                        .birthDate(LocalDate.of(1993, 05, 16))
                        .description("아이유는 대한민국의 가수이자 배우이다. 배우로 활동할 때는 본명을 사용한다.")
                        .country("대한민국")
                        .build();
            }

            @Test
            @DisplayName("200 status를 응답한다.")
            void it_responses_200_status() throws Exception {
                mockMvc.perform(put("/artists/{id}", id)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(toJson(aArtistForm("바보")))
                        )
                        .andExpect(status().isOk())
                        .andDo(document("update-artist",
                                requestFields(
                                        fieldWithPath("name").description("이름"),
                                        fieldWithPath("birthDate").description("생일"),
                                        fieldWithPath("country").description("국적"),
                                        fieldWithPath("agency").description("소속사").optional(),
                                        fieldWithPath("description").description("설명").optional()
                                )
                        ));
            }
        }

        @Nested
        @DisplayName("artist가 존재하지 않으면")
        class Context_when_artist_does_not_exists {
            @Test
            @DisplayName("404 status를 응답한다.")
            void it_responses_404_status() throws Exception {
                mockMvc.perform(put("/artists/{id}", 1014)
                                .content(toJson(aArtistForm()))
                                .contentType(MediaType.APPLICATION_JSON)
                        )
                        .andExpect(status().isNotFound());
            }
        }

        @Nested
        @DisplayName("유효하지 않은 요청 메시지면")
        class Context_when_invalid_request {

            ArtistForm invalidForm;

            @BeforeEach
            void setUp() {
                invalidForm = ArtistForm.builder()
                        .name(" ")
                        .country(" ")
                        .birthDate(null)
                        .build();
            }

            @Test
            @DisplayName("400 status를 응답한다.")
            void it_responses_400_status() throws Exception {
                mockMvc.perform(put("/artists/{id}", 1)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(toJson(invalidForm))
                        )
                        .andExpect(status().isBadRequest());
            }
        }
    }

    @Nested
    @DisplayName("DELETE /artists/{id} 요청은")
    class Describe_remove {

        @Nested
        @DisplayName("정상적인 요청이 오면")
        class Context_when_valid_request {

            Long id;

            @BeforeEach
            void setUp() {
                id = artistService.createArtist(aArtistForm());
            }

            @Test
            @DisplayName("200 status를 응답한다.")
            void it_responses_200_status() throws Exception {
                mockMvc.perform(delete("/artists/{id}", id))
                        .andExpect(status().isOk())
                        .andDo(document("delete-artist"));
            }
        }

        @Nested
        @DisplayName("artist가 존재하지 않으면")
        class Context_when_artist_does_not_exists {

            @Test
            @DisplayName("404 status를 응답한다.")
            void it_responses_404_status() throws Exception {
                mockMvc.perform(delete("/artists/{id}", 1010))
                        .andExpect(status().isNotFound());
            }
        }
    }

}