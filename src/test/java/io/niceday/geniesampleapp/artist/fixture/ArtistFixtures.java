package io.niceday.geniesampleapp.artist.fixture;

import io.niceday.geniesampleapp.artist.dto.ArtistForm;

import java.time.LocalDate;

public class ArtistFixtures {
    public static ArtistForm aArtistForm() {
        return ArtistForm.builder()
                .name("하이유")
                .agency("JPA")
                .country("대한민국")
                .birthDate(LocalDate.of(1995, 03, 29))
                .description("안녕하세요 하이유 입니다.")
                .build();
    }

    public static ArtistForm aArtistForm(String name) {
        return ArtistForm.builder()
                .name(name)
                .agency("JPA")
                .country("대한민국")
                .birthDate(LocalDate.of(1995, 03, 29))
                .description("안녕하세요 하이유 입니다.")
                .build();
    }
}
