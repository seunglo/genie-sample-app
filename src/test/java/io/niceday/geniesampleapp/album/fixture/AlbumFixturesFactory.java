package io.niceday.geniesampleapp.album.fixture;

import io.niceday.geniesampleapp.album.dto.AlbumForm;
import io.niceday.geniesampleapp.album.service.AlbumService;
import io.niceday.geniesampleapp.artist.service.ArtistService;
import org.springframework.stereotype.Component;

import static io.niceday.geniesampleapp.artist.fixture.ArtistFixtures.aArtistForm;

@Component
public class AlbumFixturesFactory {

    private final AlbumService albumService;

    private final ArtistService artistService;

    public AlbumFixturesFactory(AlbumService albumService, ArtistService artistService) {
        this.albumService = albumService;
        this.artistService = artistService;
    }

    public Long createAlbum(AlbumForm albumForm) {
        Long artistId = artistService.createArtist(aArtistForm());
        albumForm.setArtistId(artistId);
        return albumService.createAlbum(albumForm);
    }
}
