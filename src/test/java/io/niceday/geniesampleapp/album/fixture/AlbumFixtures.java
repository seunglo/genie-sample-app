package io.niceday.geniesampleapp.album.fixture;

import io.niceday.geniesampleapp.album.dto.AlbumForm;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class AlbumFixtures {
     public static AlbumForm aAlbumForm() {
        AlbumForm.Soundtrack soundtrack = AlbumForm.Soundtrack
                .builder()
                .name("라일락")
                .isDisplay(true)
                .order(1)
                .playTime(LocalTime.of(0, 3, 40))
                .build();
        return AlbumForm.builder()
                .name("IU 5th Album LILAC")
                .genre("댄스")
                .releaseDate(LocalDate.of(1993, 5, 16))
                .description("섬세하고 아련한 감성을 노래하는 아티스트 아이유")
                .soundtracks(List.of(soundtrack))
                .artistId(1L)
                .build();
    }

    public static AlbumForm aAlbumForm(String albumName) {
        AlbumForm.Soundtrack soundtrack = AlbumForm.Soundtrack
                .builder()
                .name("라일락")
                .isDisplay(true)
                .order(1)
                .playTime(LocalTime.of(0, 3, 40))
                .build();
        return AlbumForm.builder()
                .name(albumName)
                .genre("댄스")
                .releaseDate(LocalDate.of(1993, 5, 16))
                .description("섬세하고 아련한 감성을 노래하는 아티스트 아이유")
                .soundtracks(List.of(soundtrack))
                .artistId(1L)
                .build();
    }
}
