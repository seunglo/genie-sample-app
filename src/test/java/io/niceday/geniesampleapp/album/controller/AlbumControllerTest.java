package io.niceday.geniesampleapp.album.controller;

import io.niceday.geniesampleapp.album.dto.AlbumForm;
import io.niceday.geniesampleapp.album.dto.AlbumForm.Soundtrack;
import io.niceday.geniesampleapp.album.fixture.AlbumFixturesFactory;
import io.niceday.geniesampleapp.artist.service.ArtistService;
import io.niceday.geniesampleapp.common.AbstractBaseControllerTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.IntStream;

import static io.niceday.geniesampleapp.album.fixture.AlbumFixtures.aAlbumForm;
import static io.niceday.geniesampleapp.artist.fixture.ArtistFixtures.aArtistForm;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AlbumControllerTest extends AbstractBaseControllerTest {

    @Autowired
    AlbumFixturesFactory albumFixturesFactory;

    @Autowired
    ArtistService artistService;

    @Nested
    @DisplayName("GET /albums/{id} 요청은")
    class Describe_get {

        @Nested
        @DisplayName("올바른 요청이 주어지면")
        class Context_when_valid_request {
            Long id;

            @BeforeEach
            void setUp() {
                id = albumFixturesFactory.createAlbum(aAlbumForm());
            }

            @Test
            @DisplayName("albumDetail을 응답한다.")
            void it_responses_albumDetail() throws Exception {
                mockMvc.perform(get("/albums/{id}", id))
                        .andExpect(status().isOk())
                        .andDo(document("get-album",
                                responseFields(
                                        fieldWithPath("id").description("아이디"),
                                        fieldWithPath("name").description("이름"),
                                        fieldWithPath("genre").description("장르").optional(),
                                        fieldWithPath("releaseDate").description("발매일"),
                                        fieldWithPath("description").description("설명").optional(),
                                        fieldWithPath("createdBy").description("등록자"),
                                        fieldWithPath("createdDateTime").description("등록일시"),
                                        fieldWithPath("artist.id").description("아티스트 이름"),
                                        fieldWithPath("artist.name").description("아티스트 이름"),
                                        fieldWithPath("soundtracks[].id").description("음원 아이디"),
                                        fieldWithPath("soundtracks[].name").description("음원 이름"),
                                        fieldWithPath("soundtracks[].order").description("음원 순서"),
                                        fieldWithPath("soundtracks[].isDisplay").description("음원 노출 여부"),
                                        fieldWithPath("soundtracks[].playTime").description("음원 시간")
                                )
                        ));
            }
        }

        @Nested
        @DisplayName("album이 존재하지 않으면")
        class Context_when_album_does_not_exist {

            @Test
            @DisplayName("404 status를 응답한다.")
            void it_responses_404_status() throws Exception {
                mockMvc.perform(get("/albums/{id}", 123123))
                        .andExpect(status().isNotFound());
            }
        }
    }

    @Nested
    @DisplayName("GET /albums 요청은")
    class Describe_getPage {

        @Nested
        @DisplayName("앨범 이름이 라일락이 3개 포함된 6개의 데이터이가 주어지고 쿼리스트링의 name 값이 라일락이면")
        class Context_with_name_search {
            @BeforeEach
            void setUp() {
                albumFixturesFactory.createAlbum(aAlbumForm("라일락123"));
                albumFixturesFactory.createAlbum(aAlbumForm("라일락1"));
                albumFixturesFactory.createAlbum(aAlbumForm("라일락3"));
                albumFixturesFactory.createAlbum(aAlbumForm("일락"));
                albumFixturesFactory.createAlbum(aAlbumForm("라일"));
                albumFixturesFactory.createAlbum(aAlbumForm("일"));
            }

            @Test
            @DisplayName("앨범의 이름이 라일락이 포함된 데이터 3개가 조회 된다.")
            void it_responses_albumDetail() throws Exception {
                mockMvc.perform(get("/albums")
                                .queryParam("name", "라일락")
                        )
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("content.length()").value(3))
                        .andExpect(jsonPath("content[0].name").value(containsString("라일락")))
                        .andExpect(jsonPath("content[1].name").value(containsString("라일락")))
                        .andExpect(jsonPath("content[2].name").value(containsString("라일락")));

            }
        }

        @Nested
        @DisplayName("데이터가 50개가 주어지고 쿼리스트링의 page 3, size 10 이면")
        class Context_with_paging {

            @BeforeEach
            void setUp() {
                IntStream.range(0, 50)
                        .forEach((i) -> albumFixturesFactory.createAlbum(aAlbumForm("라일락" + i)));
            }

            @Test
            @DisplayName("3번째 페이지의 10개의 데이터가 조회된다.")
            void it_responses_10_data() throws Exception {
                mockMvc.perform(get("/albums")
                                .queryParam("page", "3")
                                .queryParam("size", "10")
                                .queryParam("name", "라일락")
                        )
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("content.length()").value(10))
                        .andExpect(jsonPath("number").value(3))
                        .andDo(document("page-albums",
                            requestParameters(
                                    parameterWithName("page").description("페이지 번호 (default 0)").optional(),
                                    parameterWithName("size").description("페이지 사이즈 (default 20)").optional(),
                                    parameterWithName("name").description("이름 검색 파라미터").optional()
                            )
                        ));
            }
        }
    }

    @Nested
    @DisplayName("POST /albums 요청은")
    class Describe_create {

        @Nested
        @DisplayName("올바른 요청이 주어지면")
        class Context_when_valid_request {
            AlbumForm albumForm;

            @BeforeEach
            void setUp() {
                Long artistId = artistService.createArtist(aArtistForm());
                Soundtrack soundtrack = Soundtrack
                        .builder()
                        .name("라일락")
                        .isDisplay(true)
                        .order(1)
                        .playTime(LocalTime.of(0, 3, 40))
                        .build();

                albumForm = AlbumForm.builder()
                        .name("IU 5th Album LILAC")
                        .genre("댄스")
                        .releaseDate(LocalDate.of(1993, 5, 16))
                        .description("섬세하고 아련한 감성을 노래하는 아티스트 아이유")
                        .soundtracks(List.of(soundtrack))
                        .artistId(artistId)
                        .build();
            }

            @Test
            @DisplayName("201 status를 응답한다.")
            void it_responses_201_status() throws Exception {
                mockMvc.perform(post("/albums")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(toJson(albumForm))
                        )
                        .andExpect(status().isCreated())
                        .andDo(document("create-album",
                           requestFields(
                                   fieldWithPath("name").description("이름"),
                                   fieldWithPath("genre").description("장르").optional(),
                                   fieldWithPath("description").description("설명").optional(),
                                   fieldWithPath("releaseDate").description("발매일"),
                                   fieldWithPath("artistId").description("아티스트 아이디"),
                                   fieldWithPath("soundtracks[].name").description("음원 이름"),
                                   fieldWithPath("soundtracks[].order").description("음원 순서"),
                                   fieldWithPath("soundtracks[].isDisplay").description("음원 노출여부"),
                                   fieldWithPath("soundtracks[].playTime").description("음원 시간")
                           )
                        ));
            }
        }

        @Nested
        @DisplayName("요청 바디에 필수 값이 포함되어 있지 않으면")
        class Context_when_invalid_request {
            AlbumForm albumForm;

            @BeforeEach
            void setUp() {
                AlbumForm.Soundtrack soundtrack = Soundtrack
                        .builder()
                        .build();
                albumForm = AlbumForm
                        .builder()
                        .soundtracks(List.of(soundtrack))
                        .build();
            }

            @Test
            @DisplayName("400 status를 응답한다.")
            void it_responses_400_status() throws Exception {
                mockMvc.perform(post("/albums")
                                .content(toJson(albumForm))
                                .contentType(MediaType.APPLICATION_JSON)
                        )
                        .andExpect(status().isBadRequest());
            }
        }
    }

    @Nested
    @DisplayName("PUT /albums/{id} 요청은")
    class Describe_update {

        @Nested
        @DisplayName("올바른 요청이 주어지면")
        class Context_when_valid_request {
            AlbumForm form;

            Long id;

            @BeforeEach
            void setUp() {
                form = aAlbumForm();
                id = albumFixturesFactory.createAlbum(form);
            }

            @Test
            @DisplayName("200 status 응답한다.")
            void it_responses_200_status() throws Exception {
                mockMvc.perform(put("/albums/{id}", id)
                                .content(toJson(form))
                                .contentType(MediaType.APPLICATION_JSON)
                        )
                        .andExpect(status().isOk())
                        .andDo(document("update-album",
                                requestFields(
                                        fieldWithPath("name").description("이름"),
                                        fieldWithPath("genre").description("장르").optional(),
                                        fieldWithPath("description").description("설명").optional(),
                                        fieldWithPath("releaseDate").description("발매일"),
                                        fieldWithPath("artistId").description("아티스트 아이디"),
                                        fieldWithPath("soundtracks[].name").description("음원 이름"),
                                        fieldWithPath("soundtracks[].order").description("음원 순서"),
                                        fieldWithPath("soundtracks[].isDisplay").description("음원 노출여부"),
                                        fieldWithPath("soundtracks[].playTime").description("음원 시간")
                                )
                        ));
            }
        }

        @Nested
        @DisplayName("album이 존재하지 않으면")
        class Context_when_album_does_not_exist {

            @Test
            @DisplayName("404 status를 응답한다.")
            void it_responses_404_status() throws Exception {
                mockMvc.perform(put("/albums/{id}", 1013)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(toJson(aAlbumForm()))
                        )
                        .andExpect(status().isNotFound());
            }
        }
    }

    @Nested
    @DisplayName("DELETE /albums/{id} 요청은")
    class Describe_remove {

        @Nested
        @DisplayName("올바른 요청이 주어지면")
        class Context_when_invalid_request {
            Long id;

            @BeforeEach
            void setUp() {
                id = albumFixturesFactory.createAlbum(aAlbumForm());
            }

            @Test
            @DisplayName("200 status를 응답한다.")
            void it_responses_200_status() throws Exception {
                mockMvc.perform(delete("/albums/{id}", id))
                        .andExpect(status().isOk())
                        .andDo(document("delete-album"));
            }
        }

        @Nested
        @DisplayName("album이 존재하지 않으면")
        class Context_when_album_does_not_exist {

            @Test
            @DisplayName("404 status를 응답한다.")
            void it_responses_404_status() throws Exception {
                mockMvc.perform(delete("/albums/{id}", 123123))
                        .andExpect(status().isNotFound());
            }
        }
    }
}